public class Paprika extends Täytteet{
	
	private double hinta;

	public Paprika(PizzanOsat valmisPizza) {
		super(valmisPizza);
		hinta = 1.2;
	}
	
	@Override
	public double getHinta() {
		return super.getHinta() + hinta;
	}
	
	@Override
	public String getKuvaus() {
		return super.getKuvaus() + "paprikalla ";
	}

}
