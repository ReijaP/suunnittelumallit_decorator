
public class Pohja implements PizzanOsat {
	
	private double hinta;
	
	public Pohja() {
		hinta = 3.2;
	}

	@Override
	public double getHinta() {
		return hinta;
	}

	@Override
	public String getKuvaus() {
		return "Rapea pizzapohja ";
	}

}
