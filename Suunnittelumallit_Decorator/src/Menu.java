import java.util.ArrayList;
import java.util.List;

public class Menu {
	
	private List<PizzanOsat> menu = new ArrayList<>();
	
	public void luoMenu() {
		PizzanOsat tomaattiPizza = new Juusto(new Tomaattikastike(new Pohja()));
		PizzanOsat kreikkalainenPizza = new Feta(new Paprika(new Basilika(new Tomaattikastike (new Pohja()))));
		PizzanOsat sipuliPizza = new Juusto(new Sipuli(new Tomaattikastike(new Pohja())));
		
		this.menu.add(kreikkalainenPizza);
		this.menu.add(sipuliPizza);
		this.menu.add(tomaattiPizza);
		
	}
	
	public void tulostaMenu() {
		luoMenu();
		System.out.println("Pizzerian Menu\n");
		for(int i = 0; i< menu.size(); i++) {
			System.out.println("Pizza: " + menu.get(i).getKuvaus());
			System.out.println("Hinta: " + menu.get(i).getHinta() + "€\n");
		}
	}
	

}
