public class Basilika extends Täytteet{
	
	private double hinta;

	public Basilika(PizzanOsat valmisPizza) {
		super(valmisPizza);
		hinta = 0.40;
	}
	
	@Override
	public double getHinta() {
		return super.getHinta() + hinta;
		
	}
	
	@Override
	public String getKuvaus() {
		return super.getKuvaus() + "basilikalla ";
	}

}
