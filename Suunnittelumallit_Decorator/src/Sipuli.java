public class Sipuli extends Täytteet{
	
	private double hinta;

	public Sipuli(PizzanOsat valmisPizza) {
		super(valmisPizza);
		hinta = 0.80;
	}
	
	@Override
	public double getHinta() {
		return super.getHinta() + hinta;
	}
	
	@Override
	public String getKuvaus() {
		return super.getKuvaus() + "sipulilla ";
	}

}
