public class Feta extends Täytteet{
	
	private double hinta;

	public Feta(PizzanOsat valmisPizza) {
		super(valmisPizza);
		hinta = 2.5;
	}
	
	@Override
	public double getHinta() {
		return super.getHinta() + hinta;
	}
	
	@Override
	public String getKuvaus() {
		return super.getKuvaus() + "fetalla ";
	}

}
