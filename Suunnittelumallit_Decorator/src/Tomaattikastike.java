public class Tomaattikastike extends Täytteet{
	
	private double hinta;

	public Tomaattikastike(PizzanOsat valmisPizza) {
		super(valmisPizza);
		hinta = 0.10;
	}
	
	@Override
	public double getHinta() {
		return super.getHinta() + hinta;
		
	}
	
	@Override
	public String getKuvaus() {
		return super.getKuvaus() + "tomaattikastikkeella ";
	}

}
