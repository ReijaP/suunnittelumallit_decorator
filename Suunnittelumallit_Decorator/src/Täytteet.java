

public abstract class Täytteet implements PizzanOsat {
	protected PizzanOsat valmisPizza;
	
	public Täytteet(PizzanOsat valmisPizza) {
		this.valmisPizza = valmisPizza;
	}

	@Override
	public double getHinta() {
		return valmisPizza.getHinta();
	}

	@Override
	public String getKuvaus() {
		return valmisPizza.getKuvaus();
	}

}
