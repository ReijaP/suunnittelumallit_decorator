public class Juusto extends Täytteet{
	
	private double hinta;

	public Juusto(PizzanOsat valmisPizza) {
		super(valmisPizza);
		hinta = 1.90;
	}
	
	@Override
	public double getHinta() {
		return super.getHinta() + hinta;
	}
	
	@Override
	public String getKuvaus() {
		return super.getKuvaus() + "juustolla ";
		
	}

}
